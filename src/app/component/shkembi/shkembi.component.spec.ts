import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShkembiComponent } from './shkembi.component';

describe('ShkembiComponent', () => {
  let component: ShkembiComponent;
  let fixture: ComponentFixture<ShkembiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShkembiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShkembiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
