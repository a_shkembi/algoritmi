import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShkembiIssueComponent } from './shkembi-issue.component';

describe('ShkembiIssueComponent', () => {
  let component: ShkembiIssueComponent;
  let fixture: ComponentFixture<ShkembiIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShkembiIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShkembiIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
