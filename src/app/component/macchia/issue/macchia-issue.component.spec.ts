import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MacchiaIssueComponent } from './macchia-issue.component';

describe('MacchiaIssueComponent', () => {
  let component: MacchiaIssueComponent;
  let fixture: ComponentFixture<MacchiaIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MacchiaIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MacchiaIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
