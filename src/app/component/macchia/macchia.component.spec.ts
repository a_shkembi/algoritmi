import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MacchiaComponent } from './macchia.component';

describe('MacchiaComponent', () => {
  let component: MacchiaComponent;
  let fixture: ComponentFixture<MacchiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MacchiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MacchiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
