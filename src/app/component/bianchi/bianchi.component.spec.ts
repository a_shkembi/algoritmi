import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BianchiComponent } from './bianchi.component';

describe('BianchiComponent', () => {
  let component: BianchiComponent;
  let fixture: ComponentFixture<BianchiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BianchiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BianchiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
