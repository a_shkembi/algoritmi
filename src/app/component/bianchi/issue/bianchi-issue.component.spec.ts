import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BianchiIssueComponent } from './bianchi-issue.component';

describe('BianchiIssueComponent', () => {
  let component: BianchiIssueComponent;
  let fixture: ComponentFixture<BianchiIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BianchiIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BianchiIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
