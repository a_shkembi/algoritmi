import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CiceroneIssueComponent } from './cicerone-issue.component';

describe('CiceroneIssueComponent', () => {
  let component: CiceroneIssueComponent;
  let fixture: ComponentFixture<CiceroneIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CiceroneIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CiceroneIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
