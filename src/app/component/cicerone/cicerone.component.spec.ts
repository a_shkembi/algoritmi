import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CiceroneComponent } from './cicerone.component';

describe('CiceroneComponent', () => {
  let component: CiceroneComponent;
  let fixture: ComponentFixture<CiceroneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CiceroneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CiceroneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
