import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FantiniIssueComponent } from './fantini-issue.component';

describe('FantiniIssueComponent', () => {
  let component: FantiniIssueComponent;
  let fixture: ComponentFixture<FantiniIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FantiniIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FantiniIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
