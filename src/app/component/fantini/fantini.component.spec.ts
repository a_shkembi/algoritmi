import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FantiniComponent } from './fantini.component';

describe('FantiniComponent', () => {
  let component: FantiniComponent;
  let fixture: ComponentFixture<FantiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FantiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FantiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
