export class User {
  firstName: string;
  lastName: string;
  email: string;
  path: string;
  component: any;
}
