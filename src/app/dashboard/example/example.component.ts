import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.css']
})
export class ExampleComponent implements OnInit {
  isPrime: boolean;
  numberPrime: number;
  constructor() { }

  ngOnInit() {
  }

  get checkIsPrime() {
    let divisor = 2;

    while (this.numberPrime > divisor) {
      if (this.numberPrime % divisor === 0) {
        this.isPrime = false;
        return false;
      } else {
        divisor++;
      }
    }
    this.isPrime = true;
    return true;
  }

}
