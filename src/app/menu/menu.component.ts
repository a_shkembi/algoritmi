import { Component, OnInit } from '@angular/core';
import {User} from '../model/user.model';
import {UsersService} from '../service/users.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  users: User[] = [];
  constructor(private usersService: UsersService) {
  }

  ngOnInit() {
    this.users = this.usersService.getUsers();
  }

}
