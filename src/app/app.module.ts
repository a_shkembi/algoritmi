import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing/app-routing.module';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import { MenuComponent } from './menu/menu.component';
import { MatMenuModule } from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatChipsModule} from '@angular/material/chips';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BianchiComponent } from './component/bianchi/bianchi.component';
import { CiceroneComponent } from './component/cicerone/cicerone.component';
import { FantiniComponent } from './component/fantini/fantini.component';
import { MacchiaComponent } from './component/macchia/macchia.component';
import { ShkembiComponent } from './component/shkembi/shkembi.component';
import { ExampleComponent } from './dashboard/example/example.component';
import { BianchiIssueComponent } from './component/bianchi/issue/bianchi-issue.component';
import { CiceroneIssueComponent } from './component/cicerone/issue/cicerone-issue.component';
import { FantiniIssueComponent } from './component/fantini/issue/fantini-issue.component';
import { MacchiaIssueComponent } from './component/macchia/issue/macchia-issue.component';
import { ShkembiIssueComponent } from './component/shkembi/issue/shkembi-issue.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DashboardComponent,
    BianchiComponent,
    CiceroneComponent,
    FantiniComponent,
    MacchiaComponent,
    ShkembiComponent,
    ExampleComponent,
    BianchiIssueComponent,
    CiceroneIssueComponent,
    FantiniIssueComponent,
    MacchiaIssueComponent,
    ShkembiIssueComponent
  ],
  entryComponents: [
    BianchiComponent,
    CiceroneComponent,
    FantiniComponent,
    MacchiaComponent,
    ShkembiComponent ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatChipsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
