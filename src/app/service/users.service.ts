import { Injectable } from '@angular/core';
import {User} from "../model/user.model";
import {BianchiComponent} from "../component/bianchi/bianchi.component";
import {CiceroneComponent} from "../component/cicerone/cicerone.component";
import {FantiniComponent} from "../component/fantini/fantini.component";
import {MacchiaComponent} from "../component/macchia/macchia.component";
import {ShkembiComponent} from "../component/shkembi/shkembi.component";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  users: User[] = [];
  constructor() {
    this.setUsers();
  }

  setUsers() {
    this.users = [{
      firstName: 'Valerio',
      lastName: 'Bianchi',
      email: 'v.bianchi@eustema.it',
      path: 'bianchi',
      component: BianchiComponent
    },
      {
        firstName: 'Federico',
        lastName: 'Cicerone',
        email: 'f.cicerone@eustema.it',
        path: 'cicerone',
        component: CiceroneComponent
      },
      {
        firstName: 'Antonio',
        lastName: 'Fantini',
        email: 'a.fantini@eustema.it',
        path: 'fantini',
        component: FantiniComponent
      },
      {
        firstName: 'Domenico ',
        lastName: 'Macchia',
        email: 'd.macchia@eustema.it',
        path: 'macchia',
        component: MacchiaComponent
      },
      {
        firstName: 'Alfiero',
        lastName: 'Shkembi',
        email: 'a.shkembi@eustema.it',
        path: 'shkembi',
        component: ShkembiComponent
      }];
  }

  getUsers(): User[] {
    return this.users;
  }
}
