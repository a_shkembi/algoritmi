import {Compiler, NgModule} from '@angular/core';
import {RouteConfigLoadEnd, Router, RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {UsersService} from "../service/users.service";
import {User} from "../model/user.model";

const routes: Routes = [
  {
    path: 'home',
    component: DashboardComponent,
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule {
  users: User[] = [];

  constructor(private usersService: UsersService, private router: Router, private compiler: Compiler) {
    this.users = this.usersService.getUsers();
    this.users.forEach(usr => {
      this.router.config.unshift(
        {
          path: usr.path,
          component: usr.component
        }
      );
    });
    this.router.resetConfig(this.router.config);
  }
}

